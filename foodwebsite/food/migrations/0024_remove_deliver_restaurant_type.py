# Generated by Django 2.0 on 2017-12-12 16:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0023_deliver_restaurants'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deliver',
            name='restaurant_type',
        ),
    ]
