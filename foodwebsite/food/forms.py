from django import forms
from .models import Restaurant, Food, Article

# Create the form class.
class RestaurantForm(forms.ModelForm):
	class Meta:
		model = Restaurant
		fields = ['name', 'restaurant_type', 'city', 'restaurant_logo']

class FoodForm(forms.ModelForm):
	class Meta:
		model = Food
		fields = ['restaurant', 'food_name', 'food_type', 'food_price']

class ArticleForm(forms.ModelForm):
	class Meta:
		model = Article
		fields = ['title', 'content', 'author']


class ReportForm(forms.ModelForm):
	class Meta:
		model = Food
		fields = ['restaurant']		
	CHOICES = (('yes', 'Yes'),('no', 'No'),)
	order_price = forms.ChoiceField(label='Sort (price)?', choices=CHOICES)
	total_price = forms.ChoiceField(label='Total price', choices=CHOICES)
	avg_price = forms.ChoiceField(label='Average (price)', choices=CHOICES)



		
		