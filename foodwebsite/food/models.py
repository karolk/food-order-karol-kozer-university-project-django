from django.db import models

# Create your models here.

# pokazuje zmianie, terminal: python manage.py makemigrations food
# konwertuje do tabeli sql
# dodaje wszystkie zmiany , terminal: python manage.py migrate

class Restaurant(models.Model):
     name = models.CharField(max_length=250)
     restaurant_type = models.CharField(max_length=150)
     city = models.CharField(max_length=250)
     restaurant_logo = models.CharField(max_length=1000)

     def __str__(self):
          return "{}".format(self.name)

     class Meta:
        verbose_name = "Restauracja"
        verbose_name_plural = "Restauracje"

class Food(models.Model):
     #klucz obcy do restaurant
     restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
     food_name = models.CharField(max_length=250)
     food_type = models.CharField(max_length=150)
     food_price = models.IntegerField(default=1)

     def __str__(self):
          return "{}".format(self.food_name)

     class Meta:
        verbose_name = "Potrawa"
        verbose_name_plural = "Potrawy" 

class Deliver(models.Model):
     restaurants = models.ManyToManyField(Restaurant)
     name = models.CharField(max_length=250)
     city = models.CharField(max_length=250)
     index_id = models.CharField(max_length=250, blank=False)

     def __str__(self):
          return "{}".format(self.name)

class Article(models.Model):
     title = models.CharField(max_length=60)
     content = models.TextField()
     author = models.CharField(max_length=60)

     def __str__(self):
          return "{}".format(self.title)
