# importujemy get_object_or_404 do obslugiwania bledow
from django.shortcuts import render, get_object_or_404
# importujemy z modeli Restauracje
from .models import Restaurant, Food, Article
# importujemy formularz
from .forms import RestaurantForm, FoodForm, ArticleForm, ReportForm

from django.http import HttpResponseRedirect
# Importowanie sumy, średniej
from django.db.models import Sum, Avg



# dodanie funkcji z widokiem
def index(request):
     # polaczenie z database i pobranie wszystkich restauracji z bazy danych
     all_restaurants = Restaurant.objects.all()
     content = {'all_restaurants': all_restaurants}
     # zwracamy,  importujemy templete z folderu food i informacje o restauracjach
     return render(request, 'food/index.html', content)

def detail(request, restaurant_id):
     # Jezeli adres jest niepoprawny to wyswietla sie blad
     restaurant = get_object_or_404(Restaurant, pk=restaurant_id)
     return render(request, 'food/detail.html', {'restaurant': restaurant})

def article(request):
     # polaczenie z database i pobranie wszystkich restauracji z bazy danych
     all_articles = Article.objects.all()
     content = {'all_articles': all_articles}
     # zwracamy,  importujemy templete z folderu food i informacje o restauracjach
     return render(request, 'food/article.html', content)



def add_restaurant(request):
    # jeżeli request.POST (przesłano formularz metodą POST) przetwórz formularz
    if request.method == 'POST':
        #przetwórz formularz zgodnie z danymi z POST
        form = RestaurantForm(request.POST)
        # sprawdz validace formularza:
        if form.is_valid():
            #zapisuje obiekt w bazie danych
            form.save()
            # Przekierowanie na strone glowna
            return HttpResponseRedirect('/food/add/restaurant/')

    #jeżeli nie przesłano formularza wyświetl czysty formularz
    else:
        form = RestaurantForm()

    return render(request, 'food/restaurant_form.html', {'form': form})


def add_food(request):
    # jeżeli request.POST (przesłano formularz metodą POST) przetwórz formularz
    if request.method == 'POST':
        #przetwórz formularz zgodnie z danymi z POST
        form = FoodForm(request.POST)
        # sprawdz validace formularza:
        if form.is_valid():
            #zapisuje obiekt w bazie danych
            form.save()
            # Przekierowanie na strone glowna
            return HttpResponseRedirect('/food/add/food/')

    #jeżeli nie przesłano formularza wyświetl czysty formularz
    else:
        form = FoodForm()

    return render(request, 'food/food_form.html', {'form': form})


def add_article(request):
    # jeżeli request.POST (przesłano formularz metodą POST) przetwórz formularz
    if request.method == 'POST':
        #przetwórz formularz zgodnie z danymi z POST
        form = ArticleForm(request.POST)
        # sprawdz validace formularza:
        if form.is_valid():
            #zapisuje obiekt w bazie danych
            form.save()
            # Przekierowanie na strone glowna
            return HttpResponseRedirect('/food/add/article/')

    #jeżeli nie przesłano formularza wyświetl czysty formularz
    else:
        form = ArticleForm()

    return render(request, 'food/food_form.html', {'form': form})





def get_report(request):
    # jeżeli request.POST (przesłano formularz metodą POST) przetwórz formularz
    if request.method == 'POST':
        #przetwórz formularz zgodnie z danymi z POST
        form = ReportForm(request.POST)
        # sprawdz validace formularza:
        if form.is_valid():
            restaurant_id = form.data['restaurant']
            order_price= form.data['order_price']
            total_price = form.data['total_price']
            avg_price = form.data['avg_price']
            option_select = 'yes'
            price_boolean = False
            avg_boolean = False

            # Filtorwanie - znaleznienie restauracji po id
            get_restaurant = Restaurant.objects.filter(id=restaurant_id)
            # Filtorwanie - znaleznienie potraw po id restauracji
            food_list = Food.objects.filter(restaurant = restaurant_id)

            if order_price == option_select:
                food_list = food_list.order_by('food_price')

            if total_price == option_select:
                total_price = food_list.aggregate(Sum('food_price'))
                price_boolean = True

            if avg_price == option_select:
                avg_price= food_list.aggregate(Avg('food_price'))
                avg_boolean = True

            return render(request, 'food/report.html',
            { 
                'restaurants': get_restaurant, 
                'foods': food_list, 
                'total': total_price, 
                'avg_price': avg_price , 
                'price_statement': price_boolean,
                'avg_statement': avg_boolean
            })

    #jeżeli nie przesłano formularza wyświetl czysty formularz
    else:
        form = ReportForm()

    return render(request, 'food/report_form.html', {'form': form})


