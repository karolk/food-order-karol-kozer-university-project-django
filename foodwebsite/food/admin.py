from django.contrib import admin
from .models import Restaurant, Food, Deliver, Article
# importujemy restaurant, food, deliver


class RestaurantAdmin(admin.ModelAdmin):
    list_display = ("name","city") 
    list_filter = ("restaurant_type",)
    search_fields = ("name","city")

    def get_restaurant_name(self):
        return self.restaurant.name
    get_restaurant_name.admin_order_field = 'restaurant__name'

class FoodAdmin(admin.ModelAdmin):
    list_display = ("food_name", "food_price","get_restaurant_name", "get_restaurant_city") 
    list_filter = ("food_type",)
    search_fields = ("food_name","get_restaurant_name")

    def get_restaurant_name(self, obj):
        return obj.restaurant.name
    get_restaurant_name.short_description = 'Restaurcja'
    get_restaurant_name.admin_order_field = 'restaurant__name'

    def get_restaurant_city(self, obj):
        return obj.restaurant.city
    get_restaurant_city.short_description = 'Miasto'
    get_restaurant_city.admin_order_field = 'restaurant__city'


class DeliverAdmin(admin.ModelAdmin):
    list_display = ("name", "index_id", "city") 
    list_filter = ("restaurants",)
    search_fields = ("name","city","index_id")

class ArticleAdmin(admin.ModelAdmin):
    list_display = ("title", "author") 
    list_filter = ("author",)
    search_fields = ("title", "author")

# Register your models here.
admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(Food, FoodAdmin)
admin.site.register(Deliver, DeliverAdmin)
admin.site.register(Article, ArticleAdmin)
