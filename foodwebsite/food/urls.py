from django.urls import path
from . import views
# import path urls - zaimportowanie urls
# . import views - ze wszystkich widokow

app_name = 'food'

urlpatterns = [
     # /food stworzenie strony homepage, '' - zostawiamy puste, szukamy funkcji index w views
    path('', views.index, name='index'),

    # /food/1  /food/<restaurant_id>stworzenie podstrony
    path('<int:restaurant_id>/', views.detail, name='detail'),

    path('add/restaurant/', views.add_restaurant, name='add-restaurant'),

    path('add/food/', views.add_food, name='add-food'),

    path('add/report/', views.get_report, name='add-report'),

    path('article/', views.article, name='article'),
    
    path('add/article/', views.add_article, name='add-article')

]