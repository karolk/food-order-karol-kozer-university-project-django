# Easy Order

![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Feasy-order-django%2FScreen%20Shot%202018-04-05%20at%2014.28.22.png?alt=media&token=4ee46f81-7316-43d9-9c9f-f1cbfc00aa28)
![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Feasy-order-django%2FScreen%20Shot%202018-04-05%20at%2014.29.07.png?alt=media&token=ef7fc889-03f4-4e5d-b6a4-38a682adab8f)
![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Feasy-order-django%2FScreen%20Shot%202018-04-05%20at%2014.29.18.png?alt=media&token=d5d4c127-fba4-499e-ab74-17ea620d4095)
![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Feasy-order-django%2FScreen%20Shot%202018-04-05%20at%2014.29.29.png?alt=media&token=2a2817e0-3278-453c-8e7a-a0d5be6e8fb3)

### Description

Project was made as part of "internet technology" workshops at the "Polsko Japońska Akademia Technik Komputerowych" university.

### The project includes:

Python, Django 2
